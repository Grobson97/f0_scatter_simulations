import utils.scatter_tools as scatter_tools
import numpy as np

expected_number_of_clashes = 2
f0_array = np.array([1, 3, 5, 6, 7, 9])
qr = 1
linewidth_spacing_clash_threshold = 0.2

clashes = scatter_tools.get_number_of_clashes(
    f0_array=f0_array,
    qr=qr,
    line_width_spacing_clash_threshold=linewidth_spacing_clash_threshold,
    plot_spacing=True,
)

print(f"Number of clashes: {clashes}")
print(f"Test success: {expected_number_of_clashes == clashes}")
