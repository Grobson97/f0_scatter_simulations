import utils.scatter_tools as scatter_tools
import numpy as np

f0_array = np.linspace(start=2e9, stop=2.5e9, num=390)
qr = 5e4
fab_yield = 0.9
fractional_frequency_shift = 0.001
line_width_spacing_threshold = 10
repeats = 10000

randomized_f0_array = scatter_tools.randomize_f0_array(
    f0_array=f0_array,
    fab_yield=fab_yield,
    fractional_frequency_shift=fractional_frequency_shift,
    plot_f0_arrays=True,
)

clashes = scatter_tools.get_number_of_clashes(
    f0_array=randomized_f0_array,
    qr=qr,
    line_width_spacing_clash_threshold=line_width_spacing_threshold,
    plot_spacing=True,
)

print(f"Number of clashes (1 iteration): {clashes}")

scatter_tools.get_clash_number_array(
    f0_array=f0_array,
    fab_yield=fab_yield,
    fractional_frequency_shift=fractional_frequency_shift,
    qr=qr,
    line_width_spacing_clash_threshold=line_width_spacing_threshold,
    repeats=repeats,
    plot_density_histogram=True,
)
