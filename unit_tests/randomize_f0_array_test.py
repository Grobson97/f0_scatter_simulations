import utils.scatter_tools as scatter_tools
import numpy as np


f0_array = np.array([2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0]) * 1e9
fab_yield = 1
fractional_frequency_shift = 0.001

randomized_f0_array = scatter_tools.randomize_f0_array(
    f0_array=f0_array,
    fab_yield=fab_yield,
    fractional_frequency_shift=fractional_frequency_shift,
    plot_f0_arrays=True,
)
